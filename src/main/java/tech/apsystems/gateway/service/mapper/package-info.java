/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package tech.apsystems.gateway.service.mapper;
