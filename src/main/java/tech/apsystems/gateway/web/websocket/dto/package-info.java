/**
 * Data Access Objects used by WebSocket services.
 */
package tech.apsystems.gateway.web.websocket.dto;
