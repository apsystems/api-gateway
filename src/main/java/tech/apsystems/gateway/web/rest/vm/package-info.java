/**
 * View Models used by Spring MVC REST controllers.
 */
package tech.apsystems.gateway.web.rest.vm;
